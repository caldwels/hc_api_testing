# Hockey Community Heatmap
A heatmap for indoor and outdoor hockey rinks. Also has a marker mode for finding rinks. 

Built with Vue.js and Google Maps API.

I initally wanted to make a heatmap that covered the whole country/North America, but the 100km limit prevented me from doing that without making hundreds of API calls. I see why you disabled zoom on HC Explore :)


## Getting Started
Clone the repo, and serve it with
```
 python -m SimpleHTTPServer 8080
```
or whatever you prefer. 

