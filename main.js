'use strict';

var mapStyle = [
    {
        "featureType": "administrative",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            },
            {
                "gamma": "1.00"
            }
        ]
    },
    {
        "featureType": "administrative.locality",
        "elementType": "labels",
        "stylers": [
            {
                "color": "#009546"
            }
        ]
    },
    {
        "featureType": "administrative.neighborhood",
        "elementType": "labels",
        "stylers": [
            {
                "color": "#009546"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "simplified"
            },
            {
                "lightness": "65"
            },
            {
                "saturation": "-100"
            },
            {
                "hue": "#ff0000"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "simplified"
            },
            {
                "saturation": "-100"
            },
            {
                "lightness": "80"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.attraction",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.attraction",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            },
            {
                "hue": "#ff0000"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#dd5656"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "off"
            },
            {
                "color": "#dddddd"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "saturation": "9"
            }
        ]
    },
    {
        "featureType": "road.highway.controlled_access",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "simplified"
            },
            {
                "color": "#dddddd"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            },
            {
                "color": "#ba3838"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "simplified"
            },
            {
                "color": "#eeeeee"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "hue": "#00ffc5"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#602f2f"
            },
            {
                "saturation": "-100"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "transit.station",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit.station",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#ba5858"
            },
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "transit.station",
        "elementType": "labels.icon",
        "stylers": [
            {
                "hue": "#ff0036"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#adc4cb"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "visibility": "off"
            },
            {
                "hue": "#ff0000"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    }
]
var gradient = [
    'rgba(255, 0, 0, 0)',
    'rgba(0, 254, 127, 1)',
    'rgba(0, 200, 100, 1)',
    'rgba(0, 140, 70, 1)',
    'rgba(0, 70, 35, 1)',
    'rgba(120, 120, 120, 1)',
    'rgba(60, 60, 60, 1)',
    'rgba(0, 0, 0, 1)'
];

///// Main App /////
var app = new Vue({
    el: '#app',
    data: {
        gmap: new google.maps.Map(document.getElementById('map'), {
            zoom: 12,
            center: { lat: 49.2827, lng: -123.1207 },
            mapTypeId: 'roadmap',
            scrollwheel: true,
            styles: mapStyle
        }),
        markers: [],
        loaderMessage: "",
        rink_points: [],
        rink_type: 'indoor_rinks',
        rink_type_options: [
            { text: 'Indoor Rinks', value: 'indoor_rinks' },
            { text: 'Outdoor Rinks', value: 'outdoor_rinks' }
        ],
        map_layer: 'heatmap',
        map_layer_options: [
            { text: 'Heatmap', value: 'heatmap' },
            { text: 'Markers', value: 'markers' }
        ],
        heatmap: null
    },
    mounted: function () {
        this.initMap();
    },
    computed: {
        mapCoordinates: function () {
            return {
                ne: '',
                sw: '',
                center: ''
            }
        },
    },
    methods: {
        initMap: function () {
            var _this = this;
            var minZoomLevel = 12;
            var input = document.getElementById('search-input');
            var searchBox = new google.maps.places.SearchBox(input);

            //Listeners
            //When map is idle, get rink data
            this.gmap.addListener('idle', function () {
                _this.updateMapCoordinates(_this.gmap.getBounds());
                _this.getRinkLocations();
            });
            //Listeners for search box
            this.gmap.addListener('bounds_changed', function () {
                searchBox.setBounds(_this.gmap.getBounds());
            });
            searchBox.addListener('places_changed', function () {
                var places = searchBox.getPlaces();
                if (places.length == 0) {
                    return;
                }
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function (place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                _this.gmap.fitBounds(bounds);
                _this.gmap.setZoom(12);
                _this.getRinkLocations();
            });
        },
        addHeatmap: function () {
            var pointsArray = this.rink_points.map(a => a.latlng);

            var hm = new google.maps.visualization.HeatmapLayer({
                data: pointsArray,
                radius: 30,
                gradient: gradient,
                map: this.gmap
            })
            this.heatmap = hm;
        },
        clearHeatmap: function () {
            if (this.heatmap) {
                this.heatmap.setMap(null);
            }
        },
        addMarkers: function () {
            var _this = this;
            var infoWindows = [];
            this.rink_points.forEach(element => {
                var contentString = '<div id="content">' +
                    '<h1>' + element.rink_name + '</h1>'+
                    '<h2>' + element.location + '</h2>';
                var infowindow = new google.maps.InfoWindow({
                    content: contentString
                });
                infoWindows.push(infowindow);

                var marker = new google.maps.Marker({
                    position: element.latlng,
                    map: this.gmap
                });
                marker.addListener('click', function () {
                    //close other popups
                    for (var i=0;i<infoWindows.length;i++) {
                        infoWindows[i].close();
                     }
                    infowindow.open(this.gmap, marker);
                });

                _this.markers.push(marker);
            });
        },
        clearMarkers: function () {
            for (var i = 0; i < this.markers.length; i++) {
                this.markers[i].setMap(null);
            }
            this.markers = [];
        },
        //Function that queries HC API based on map coordinates, and displays points on map
        getRinkLocations: function () {
            var _this = this;
            var minZoomLevel = 12;
            this.rink_points = [];
            if (this.gmap.getZoom() < minZoomLevel) {
                this.loaderMessage = "Zoom in to find rinks";
                return;
            }

            var ne = this.mapCoordinates.ne;
            var sw = this.mapCoordinates.sw;
            var center = this.mapCoordinates.center;
            var rink_type = this.rink_type;
            var myHeaders = new Headers({});
            this.loaderMessage = "Loading rinks...";

            //Parsing for API
            var neCoord = ne.lat() + ',' + ne.lng();
            var swCoord = sw.lat() + ',' + sw.lng();
            var centerCoord = center.lat() + ',' + center.lng();

            var myInit = {
                method: 'GET',
                headers: myHeaders,
                mode: 'cors',
                cache: 'default'
            };

            var url = 'https://api-us.hockey-community.com/v1/explore/locations?category=' + rink_type + '&sw=' + swCoord + '&ne=' + neCoord + '&origin=' + centerCoord;
            fetch(url, myInit)
                .then(response => response.json())
                .then(data => {
                    var result = data.map(a => {
                        this.rink_points.push({
                            latlng: new google.maps.LatLng(a.latitude, a.longitude),
                            rink_name: a.name,
                            location: a.friendly_location
                        })

                    });
                    if (this.map_layer === 'heatmap') {
                        this.clearMarkers();
                        //Have to redraw heatmap, it's not updating properly when points are added to the array
                        this.clearHeatmap();
                        this.addHeatmap();
                    }
                    if (this.map_layer === 'markers') {
                        this.clearHeatmap();
                        this.addMarkers();
                    }
                    this.loaderMessage = "";
                })
                .catch(error => console.error(error));
        },
        //Updates map coordinates used for API calls in getRinkLocations
        updateMapCoordinates: function (bounds) {
            this.mapCoordinates.ne = bounds.getNorthEast();
            this.mapCoordinates.sw = bounds.getSouthWest();
            this.mapCoordinates.center = bounds.getCenter();
            this.$forceUpdate();
        }
    }
})
